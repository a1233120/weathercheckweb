# 陳贈文 505170532 
# 熊宗怡 e61010003

from django.contrib import admin

from weather.models import Country

# Register your models here.

# 需要顯示在後台的欄位名稱
class CountryAdmin(admin.ModelAdmin):
    list_display = ('country_id', 'country_name', 'two_days_method', 'week_method')

# 註冊Country模型
admin.site.register(Country, CountryAdmin)
