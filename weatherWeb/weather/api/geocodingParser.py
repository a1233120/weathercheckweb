# 陳贈文 505170532 
# 熊宗怡 e61010003

# 獲取所在縣市的單例物件

import requests
import json
from weather.api import settings

class GeocodingParser():
    _instance = None
    # 單例模式公用物件
    @staticmethod
    def getInstance():
        if GeocodingParser._instance is None :
            GeocodingParser()
        return GeocodingParser._instance

    def __init__(self) -> None:
        GeocodingParser._instance = self

    # lat : float -> 緯度
    # lng : float -> 經度
    # return : 一級或二級縣市名稱, 所在鄉鎮市區名稱
    def getCountryName(self, lat, lng):
        # 取得API網址
        api_url = settings.getGeocodingUrl(lat, lng)

        # 發送請求並轉換成Python Object
        geocodingObject = json.loads( requests.get(api_url).text )

        # 檢查回應狀態
        if geocodingObject['status'] != 'OK' :
            return "", ""

        # 直接取陣列中第一個使用
        address_components = geocodingObject['results'][0]['address_components']

        # 取得Google定義中的一級(直轄市)或二級(縣)城市
        country_name = ""
        city_name = ""
        # 使用產生器函數做迭代
        def addressGenerator(addressJson):
            for i in range(1, len(addressJson)):
                yield addressJson[i]

        # 抓取一二級縣市與鄉鎮區
        for component in addressGenerator(address_components):
            # 抓取一二級縣市
            if component['types'][0] == "administrative_area_level_1" \
                or component['types'][0] == "administrative_area_level_2":
                country_name = component['long_name']
            # 抓取鄉鎮區
            if component['types'][0] == "administrative_area_level_3":
                city_name = component['long_name']

        return country_name, city_name