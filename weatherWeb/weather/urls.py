# 陳贈文 505170532 
# 熊宗怡 e61010003

from django.urls import path
from . import views

urlpatterns = [
    # router : '' , Url模式
    # view : 檢測到Url模式為''時，會調用views中的index函數
    # name : 映射該Url的唯一標示，可以透過全局使用Url 'index'，來連結到這個網頁
    path('', views.index, name='index'),
    path('weatherBylatAndlng', views.weatherBylatAndlng)
]