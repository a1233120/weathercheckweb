# 陳贈文 505170532 
# 熊宗怡 e61010003

from django.apps import AppConfig


class WeatherConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'weather'
