# 陳贈文 505170532 
# 熊宗怡 e61010003

from django.shortcuts import render
from weather.api import settings
from weather.api.geocodingParser import GeocodingParser

from weather.api.weatherReporter import WeatherReporter

# Create your views here.

def index(request):
    """View function for home page of site."""
    context = {
        'title' : '天氣預測網站',
        'date' : datetime.now().time()
    }

    return render(request, 'index.html', context=context)

# 用於網頁端使用Ajax使用
from django.http import HttpResponse
from datetime import datetime, timedelta
import json

# 獲得天氣狀況
def weatherBylatAndlng(request):
    # 獲得請求的經緯度位置
    lat = float(request.GET['lat'])
    lng = float(request.GET['lng'])

    # 獲取城市名稱(透過Google Api進行取得)
    country_name, city_name = GeocodingParser.getInstance().getCountryName(lat, lng)

    # 獲取天氣報告(透過氣象開放平台Api進行取得)
    weather_report = WeatherReporter.getInstance().getReporter(country_name, city_name, False)

    # 回應內容
    context = {
        'country' : country_name,
        'city' : city_name,
        'repoter' : weather_report,
    }

    # ensure_ascii : 避免給予encode過的中文字
    return HttpResponse(json.dumps(context, ensure_ascii=False), content_type='application/json')