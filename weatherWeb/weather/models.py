from django.db import models
# 陳贈文 505170532 
# 熊宗怡 e61010003

from django.db.models import constraints

# Create your models here.

# 台灣各城鎮API儲存
class Country(models.Model):
    # 此為映射到資料表中的資料型別與資料欄位名稱
    country_id = models.IntegerField(help_text="城鎮編號", primary_key=True)
    country_name = models.CharField(max_length=10)
    two_days_method = models.CharField(max_length=50, help_text="兩天的API方法")
    week_method = models.CharField(max_length=50, help_text="一週的API方法")

    def __str__(self):
        return self.country_name