# 陳贈文 505170532 
# 熊宗怡 e61010003

# 氣象開放平台API BASE URL
API_BASE_URL = 'https://opendata.cwb.gov.tw/api/v1/rest/datastore/'

# 用於從[中央氣象局氣象資料開放平台](http://opendata.cwb.gov.tw/usages)取得資料，申辦完會員後即可取得
WEATHER_AUTHORIZATION_KEY = 'CWB-49EFBFB4-7F88-4F7F-BA19-13C10BA6676A'

# 獲得依據功能轉換URL(目前只接收天氣現象及溫度)
# apiMethod : string -> 資料庫內的method分類字串
def getConversionUrl(apiMethod):
    return API_BASE_URL + apiMethod + '?' + 'Authorization=' + WEATHER_AUTHORIZATION_KEY + '&format=JSON&elementName=Wx,T'

# Google Geocoding API Authorization Key(只限定用於Geocoding Api)
GOOGLE_GEOCODING_API_AUTHORIZATION_KEY = 'AIzaSyBau0iwCGu03DLLVoS-rp-OfvYmFx_6jE4'

# Google Geocoding API
GOOGLE_GEOCODING_API = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&language=zh-TW&key=' + GOOGLE_GEOCODING_API_AUTHORIZATION_KEY

# 獲得完整取得地理位址Url
# lat : float -> 緯度
# lng : float -> 經度
# 限制非合理值的話回傳空字串，避免浪費API資源(要錢)
def getGeocodingUrl(lat, lng):
    if lat > 90 and lat < -90:
        return ""
    
    if lng > 180 and lng < -180:
        return ""

    return GOOGLE_GEOCODING_API % ( lat, lng )