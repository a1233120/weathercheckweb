# 陳贈文 505170532 
# 熊宗怡 e61010003

"""
ASGI config for weatherWeb project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'weatherWeb.settings')

application = get_asgi_application()
