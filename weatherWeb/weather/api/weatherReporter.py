# 獲取天氣情報的單例物件

import requests
import json
# 陳贈文 505170532 
# 熊宗怡 e61010003

from weather.models import Country
from weather.api import settings

class WeatherReporter():
    _instance = None
    # 單例模式公用物件
    @staticmethod
    def getInstance():
        if WeatherReporter._instance is None :
            WeatherReporter()
        return WeatherReporter._instance

    def __init__(self) -> None:
        WeatherReporter._instance = self

    # country_name : string -> 直轄市或其他縣市中文名稱
    # city_name : string -> 鄉鎮區中文名稱
    # isWeek : boolean -> 是否抓取一週的資料(False的話為兩天)
    def getReporter(self, country_name, city_name, isWeek):
        # 根據是否要一週來決定method
        _countrys = Country.objects.filter(country_name__iexact = country_name)
        if _countrys.count() is 0:
            return ""
        
        # 決定要用哪個API Method
        _country = _countrys[0]
        _api_method = _country.two_days_method
        if isWeek :
            _api_method = _country.week_method

        # 將Json字串轉換回Python Object
        report = json.loads( requests.get(settings.getConversionUrl(_api_method)).text )

        # 找出匹配的鄉鎮區域報告
        locationReport = None
        alllocations = report['records']['locations'][0]['location']
        # 利用過濾器物件進行過濾(找出正確地點的報告)
        locationReport = list(filter(lambda location : location['locationName'] == city_name, alllocations))

        # 回傳報告( 建議再處理地區 )
        return locationReport[0]['weatherElement']